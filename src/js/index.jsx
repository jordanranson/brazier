'use strict';

// dependencies
import React    from 'react/addons';
import Config   from './config';

// layouts
import Header   from './layouts/header';
import Page     from './layouts/page';

// Containers
let $header = $('.x-header');
let $page = $('.x-page');

// Members
let appRef = new Firebase(Config.firebasePath+'/app');
let pagesRef = new Firebase(Config.firebasePath+'/pages');
let allPages = {};


/*
 * Routing
 */

let routes = {};
let lastPath = null;
let pageRef = null;

window.addEventListener('popstate', function(event) {
    if(event.state) {
        let path = window.location.pathname;
        path = path.replace(Config.basePath, '');

        navigate(path);
    }
});

function navigate(path, done = () => {}) {
    if(lastPath !== path) {
        if(pageRef) pageRef.off();

        let pageId = routes[path];
        let statePath = Config.basePath+path;

        let ref = new Firebase(Config.firebasePath+'pages/');
        pageRef = ref.child(pageId);
        pageRef.once('value', snapshot => {
            let state = snapshot.val();

            console.log(Config.firebasePath+'pages/'+pageId);

            if(!state) {
                React.render(<span>Error</span>, $page[0]);
                return;
            }

            window.history.replaceState({ pageId }, state.title, statePath);

            React.render(<Page key={pageId} pageId={pageId} />, $page[0]);
        });

        lastPath = path;
        done();
    }
}

document.body.addEventListener('click', event => {
    let $target = $(event.target).closest('a');
    if($target.length > 0 && !$target.attr('target')) {
        let path = $target.attr('href');
        path = '/'+path;

        navigate(path, () => {
            let pageId = routes[path];
            let statePath = Config.basePath + path;

            window.history.pushState({ pageId }, document.title, statePath);
        });

        return event.preventDefault();
    }
});


/*
 * Header and route setup
 */

appRef.on('value', snapshot => {
    let newState = snapshot.val();

    // create routes
    routes = {};
    for(let key in newState.pages) {
        let page = newState.pages[key];
        let path = '/'+page.slug;

        routes[path] = key;
    }

    React.render(<Header title={newState.title} pages={newState.pages} />, $header[0]);
    allPages = newState.pages;

    let currentPath = window.location.pathname;
    if(currentPath === '') currentPath = '/';
    currentPath = currentPath.replace(Config.basePath, '');

    navigate(currentPath, () => {
        let pageId = routes[currentPath];
        let statePath = Config.basePath + currentPath;

        window.history.pushState({ pageId }, document.title, statePath);
    });
});


/*
 * Page
 */

$('.x-add-page').click(e => {
    let items = allPages;
    let order = 900000000;
    for(let key in items) {
        let item = items[key];
        if(item.order < order) order = item.order;
    }

    let title = prompt('Page title');
    let slug = prompt('Page slug');
    let published = true;
    let type = 'fit';

    if(!title || !slug) return;

    // add new page
    let newPage = { title, slug, published, type, order };
    let newPageRef = pagesRef.push(newPage);
    let newPageId = newPageRef.key();

    // update list of pages
    let newNavItem = { title, slug, published, order };
    let allPagesRef = appRef.child('pages');
    allPages[newPageId] = newNavItem;
    allPagesRef.update(allPages);
});