'use strict';

import React    from 'react';


class Header extends React.Component {
    render() {
        let _p = this.props;
        let _s = this.state;

        let menuItems = this.forEachMenuItem(_p.pages);

        return (
            <header className="main-header">
                <h1>{_p.title}</h1>
                <nav>
                    <div className="main-nav">
                        {menuItems}
                    </div>
                    <div className="dropdown-nav" style={{display:'none'}}>
                        <div className="dropdown-nav-button">
                            <i className="icon icon-menu">=</i>
                        </div>
                        <div className="dropdown-nav-content">
                            {menuItems}
                        </div>
                    </div>
                </nav>
            </header>
        );
    }

    forEachMenuItem(items) {
        let els = [];
        for(let key in items) {
            let item = items[key];
            els.push(<a key={key} href={item.slug}>{item.title}</a>);
        }
        return els;
    }
}

export default Header;