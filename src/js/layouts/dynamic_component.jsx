'use strict';

import React            from 'react/addons';
import Config           from '../config';
import StateReference   from '../state_reference';


class DynamicComponent extends React.Component {
    constructor(props) {
        super(props);

        this.path = false;
    }


    // state ref management

    componentWillMount() {
        this.stateRef = new Firebase(`${Config.firebasePath}${this.path}`);
        this.stateRef.on('value', snapshot => {
            let state = snapshot.val();

            this.onStateChange(state);
            this.setState(state);
        });
    }

    componentWillUnmount() {
        this.stateRef.off();
    }

    onStateChange(newState) {}


    // render management

    render() {
        if(this.state) return this.onRender();
        else return this.onLoading();
    }

    onRender() {
        return <span>Done</span>;
    }

    onLoading() {
        return <span>Loading</span>;
    }

    scrollTo(id) {
        let $target = $('#'+id);
        $('body').animate({
            scrollTop: $target.offset().top
        }, 1000);
    }


    /* ref management helper methods */

    prepend(newItem, items, path, scrollTo) {
        for(let key in items) {
            items[key].order++;
        }

        newItem.order = 0;

        let ref = this.stateRef.child(path);
        ref.update(items, error => {
            if(error) console.log(error);
        });

        let newRef = ref.push(newItem, err => {
            if(scrollTo) this.scrollTo(newRef.key());
        });

        return newRef.key();
    }

    append(newItem, items, path, scrollTo) {
        let order = 9000;
        for(let key in items) {
            let item = items[key];
            if(item.order < order) order = item.order;
        }

        newItem.order = order+1;

        let ref = this.stateRef.child(path);
        ref.update(items, error => {
            if(error) console.log(error);
        });

        let newRef = ref.push(newItem, err => {
            if(scrollTo) this.scrollTo(newRef.key());
        });

        return newRef.key();
    }

    insertAfter(newItem, id, items, path, scrollTo) {
        let foundItem = null;
        for(let key in items) {
            let item = items[key];
            if(key === id) {
                foundItem = item;
                break;
            }
        }

        if(!foundItem) {
            console.log('No item found');
            return;
        }

        for(let key in items) {
            let item = items[key];
            if(item.order > foundItem.order) {
                item.order++;
            }
        }

        newItem.order = foundItem.order+1;

        let ref = this.stateRef.child(path);
        ref.update(items, error => {
            if(error) console.log(error);
        });

        let newRef = ref.push(newItem, err => {
            if(scrollTo) this.scrollTo(newRef.key());
        });

        return newRef.key();
    }

    remove(id, items, path) {
        let foundItem = null;
        for(let key in items) {
            let item = items[key];
            if(key === id) {
                foundItem = item;
                break;
            }
        }

        if(!foundItem) {
            console.log('No item found');
            return;
        }

        for(let key in items) {
            let item = items[key];
            if(item.order > foundItem.order) {
                item.order--;
            }
        }

        let ref = this.stateRef.child(path);
        ref.update(items, error => {
            if(error) console.log(error);
        });

        let refItem = ref.child(id);
        refItem.remove();
    }

    update(id, item, path) {
        let ref = this.stateRef.child(`${path}/${id}`);
        ref.update(item);
    }
}

export default DynamicComponent;