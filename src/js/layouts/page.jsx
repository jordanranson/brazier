'use strict';

import React            from 'react/addons';
import Config           from '../config';
import DynamicComponent from './dynamic_component';

let Transition = React.addons.CSSTransitionGroup;


class Page extends DynamicComponent {
    constructor(props) {
        super(props);

        this.path = `pages/${this.props.pageId}`;
    }


    // state ref management

    onStateChange(newState) {
        this.sections = newState.sections;
    }


    // render management

    onRender() {
        let state = this.state;
        let sections = this.makeSections();

        return (
            <div className={`page page-${state.type}`}>
                <Transition component="div" transitionName="section-anim" className="section-group">
                    {sections}
                </Transition>
            </div>
        );
    }

    onLoading() {
        return <span>Loading</span>
    }


    // generators

    makeSections() {
        let props = this.props;
        let sectionData = this.sections;

        // Create array of sections and sort by order
        let sectionArr = [];
        for(let key in sectionData) {
            sectionArr.push({key: key, section: sectionData[key] });
        }
        sectionArr.sort((a, b) => a.section.order - b.section.order);

        // Create section markup
        let sections = [];
        for(let i = 0; i < sectionArr.length; i++) {
            let section = sectionArr[i].section;
            let key = sectionArr[i].key;
            let sectionRows = this.makeRows(key, section.rows);

            let sectionEl =
                <div id={key} key={key} className={`section-wrapper section-${section.type}`} style={{order: section.order}}>
                    <div className="section">
                        <Transition component="div" transitionName="row-anim" className="row-group">
                            {sectionRows}
                        </Transition>
                        <div className="row-add" onClick={e => this.prependRow(section.rows, key)}></div>
                    </div>
                    <div className="section-add" onClick={e => this.insertSection(key)}></div>
                </div>;

            sections.push(sectionEl);
        }

        return sections;
    }

    makeRows(sectionId, rowData) {
        // Create array of sections and sort by order
        let rowArr = [];
        for(let key in rowData) {
            rowArr.push({key: key, row: rowData[key] });
        }
        rowArr.sort((a, b) => a.row.order - b.row.order);

        // Create section markup
        let rows = [];
        for(let i = 0; i < rowArr.length; i++) {
            let row = rowArr[i].row;
            let key = rowArr[i].key;
            let cols = this.makeColumns(key, sectionId, row.cols);

            let rowEl = <div id={key} key={key} className="row-wrapper" style={{order: row.order}}>
                <div className="row">
                    <Transition component="div" transitionName="column-anim" className="column-group">
                        {cols}
                    </Transition>
                    <div className="column-add" onClick={e => this.prependColumn(row.cols, key, sectionId)}></div>
                    <div className="row-add" onClick={e => this.insertRow(key, rowData, sectionId)}></div>
                </div>
            </div>;

            rows.push(rowEl);
        }
        return rows;
    }

    makeColumns(rowId, sectionId, colData) {

        // Create array of sections and sort by order
        let colArr = [];
        for(let key in colData) {
            colArr.push({key: key, row: colData[key] });
        }
        colArr.sort((a, b) => a.row.order - b.row.order);

        // Create section markup
        let cols = [];
        for(let i = 0; i < colArr.length; i++) {
            let column = colArr[i].col;
            let key = colArr[i].key;

            let colEl = <div id={key} key={key} className="column-wrapper">
                <div className="column">
                    <div className="column-remove" onClick={e => this.removeColumn(key, colData, rowId, sectionId)}><i>&times;</i></div>
                    <div className="column-add" onClick={e => this.insertColumn(key, colData, rowId, sectionId)}></div>
                </div>
            </div>;

            cols.push(colEl);
        }
        return cols;
    }


    /* event handlers */

    makeSection() {
        let background_color = '#fff';
        let background_image = '#fff';
        let order = 0;
        let type = 'flow';

        return {
            background_color, background_image, order, type
        }
    }

    prependSection() {
        let newRow = this.makeRow();
        newRow.cols = {};
        newRow.cols['_0000'] = this.makeColumn();
        newRow.cols['_0000'].order = 0;

        let newSection = this.makeSection();
        newSection.rows = {};
        newSection.rows['_0000'] = newRow;
        newSection.rows['_0000'].order = 0;

        this.prepend(newSection, this.sections, 'sections', true);
    }

    insertSection(id) {
        let newRow = this.makeRow();
        newRow.cols = {};
        newRow.cols['_0000'] = this.makeColumn();
        newRow.cols['_0000'].order = 0;

        let newSection = this.makeSection();
        newSection.rows = {};
        newSection.rows['_0000'] = newRow;
        newSection.rows['_0000'].order = 0;

        this.insertAfter(newSection, id, this.sections, 'sections', true);
    }

    removeSection(id) {
        this.remove(id, this.sections, 'sections');
    }

    makeRow() {
        let type = 'solid';

        return {
            type
        };
    }

    prependRow(rows, sectionId) {
        let newRow = this.makeRow();
        newRow.cols = {};
        newRow.cols['_0000'] = this.makeColumn();
        newRow.cols['_0000'].order = 0;

        this.prepend(newRow, rows || {}, `sections/${sectionId}/rows`, true);
    }

    insertRow(id, rows, sectionId) {
        let newRow = this.makeRow();
        newRow.cols = {};
        newRow.cols['_0000'] = this.makeColumn();
        newRow.cols['_0000'].order = 0;

        this.insertAfter(newRow, id, rows || {}, `sections/${sectionId}/rows`, true);
    }

    removeRow(id, rows, sectionId) {
        this.remove(id, rows, `sections/${sectionId}/rows`);
    }

    makeColumn() {
        return {};
    }

    prependColumn(cols, rowId, sectionId) {
        let newCol = this.makeColumn();
        this.prepend(newCol, cols || {}, `sections/${sectionId}/rows/${rowId}/cols`);
    }

    insertColumn(id, cols, rowId, sectionId) {
        let newCol = this.makeColumn();
        this.insertAfter(newCol, id, cols || {}, `sections/${sectionId}/rows/${rowId}/cols`);
    }

    removeColumn(id, cols, rowId, sectionId) {
        let rows = this.sections[sectionId].rows;

        if(Object.keys(rows).length === 1 && Object.keys(cols).length === 1) {
            this.remove(sectionId, this.sections, 'sections');
        }
        else if(Object.keys(cols).length === 1) {
            this.remove(rowId, rows, `sections/${sectionId}/rows`);
        }
        else {
            this.remove(id, cols, `sections/${sectionId}/rows/${rowId}/cols`);
        }
    }
}

export default Page;