'use strict';

import Config from './config';

class StateReference {
    constructor(path, items) {
        this.ref = new Firebase(`${Config.firebasePath}${path}`);
        this.ref.on
        this.items = items;
    }

    prepend(newItem) {
        let items = this.items;

        for(let key in items) {
            items[key].order++;
        }

        newItem.order = 0;

        this.ref.set(items, error => {
            if(error) console.log(error);
            else this.ref.push(newItem);
        });
    }

    append(newItem) {
        let items = this.items;

        let order = 900000000;
        for(let key in items) {
            let item = items[key];
            if(item.order < order) order = item.order;
        }

        newItem.order = order+1;

        this.ref.set(items, error => {
            if(error) console.log(error);
            else this.ref.push(newItem);
        });
    }

    insertAfter(newItem, id) {
        let items = this.items;

        let foundItem = null;
        for(let key in items) {
            let item = items[key];
            if(key === id) {
                foundItem = item;
                break;
            }
        }

        if(!foundItem) {
            console.log('No item found');
            return;
        }

        for(let key in items) {
            let item = items[key];
            if(item.order > foundItem.order) {
                item.order++;
            }
        }

        newItem.order = foundItem.order+1;

        this.ref.set(items, error => {
            if(error) console.log(error);
            else this.ref.push(newItem);
        });
    }

    remove(id) {
        let items = this.items;

        let foundItem = null;
        for(let key in items) {
            let item = items[key];
            //console.log(key, id);
            if(key === id) {
                foundItem = item;
                break;
            }
        }

        if(!foundItem) {
            console.log('No item found');
            return;
        }

        for(let key in items) {
            let item = items[key];
            if(item.order > foundItem.order) {
                item.order--;
            }
        }

        delete items[id];

        this.ref.set(items, error => {
            if(error) console.log(error);
        });
    }
}


export default StateReference;