var path = require('path');
var gulp = require('gulp');
var less = require('gulp-less');
var colors = require('colors');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var LessPluginCleanCSS = require('less-plugin-clean-css');
var LessPluginAutoPrefix = require('less-plugin-autoprefix');
var cleancss = new LessPluginCleanCSS({ advanced: true });
var autoprefix = new LessPluginAutoPrefix({ browsers: ["last 2 versions"] });


function buildJsx() {
    console.log('[ ------ ] '.gray + 'Building jsx...'.yellow);
    browserify({
        entries: './src/js/index.jsx',
        extensions: ['.jsx'],
        debug: true
    })
    .transform(babelify)
    .bundle()
    .pipe(source('./brazier.js'))
    .pipe(gulp.dest('.'));
}

function buildLess() {
    console.log('[ ------ ] '.gray + 'Building less...'.yellow);
    gulp.src('./src/less/brazier.less')
    .pipe(less({
        plugins: [autoprefix, cleancss],
        paths: [ './src/less/' ]
    }))
    .pipe(gulp.dest('.'));
}


gulp.task('build-jsx', buildJsx);
gulp.task('build-less', buildLess);

gulp.task('build', function () {
    console.log('[ ------ ] '.gray + 'Building files...'.yellow);
    buildLess();
    buildJsx();
});

gulp.task('watch', function() {
    console.log('[ ------ ] '.gray + 'Watching files...'.yellow);
    gulp.watch(['src/js/**/*.jsx'], ['build-jsx']);
    gulp.watch(['src/less/**/*.less'], ['build-less']);
});

gulp.task('default', ['build', 'watch'], function() {});